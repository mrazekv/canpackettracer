﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CAN2USB;

namespace CANPacketTracer
{
    /// <summary>
    /// Interaction logic for AddCAN.xaml
    /// </summary>
    public partial class AddCAN : Window
    {
        private CAN2USB.USB device;

        public AddCAN(CAN2USB.USB device)
        {
            this.device = device;
            InitializeComponent();
            txtAddr.Focus();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Test validity of data
                foreach (char c in txtAddr.Text)
                {
                    if (!IsHex(c))
                    {
                        throw new ApplicationException("Address is not valid HEX number");
                    }
                }

                if (txtData.Text.Length % 2 == 1)
                    throw new ApplicationException("Data count is odd");

                foreach (char c in txtData.Text)
                {
                    if (!IsHex(c))
                    {
                        throw new ApplicationException("Data is not valid HEX number");
                    }
                }

                CanPacket packet = new CanPacket(true);
                packet.StdId = Convert.ToUInt32(txtAddr.Text, 16);
                packet.DLC = (byte)(txtData.Text.Length / 2);
                for (int i = 0; i < packet.DLC; i++)
                {
                    packet.Data[i] = Convert.ToByte(txtData.Text.Substring(i * 2, 2), 16);
                }
                device.SendPacket(packet);
               // Close();
            }
            catch (Exception err)
            {
                ErrHandler.Handle(err);
            }
        }

        public static bool IsHex(char c)
        {
            return Char.IsDigit(c) || (Char.ToUpper(c) >= 'A' && Char.ToUpper(c) <= 'F');
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
