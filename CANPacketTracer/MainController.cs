﻿//#define PREFILL

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using CAN2USB;
using System.Windows;
using System.ComponentModel;

namespace CANPacketTracer
{
    public class MainController : INotifyPropertyChanged
    {
        public ObservableCollection<CanPacket> ViewPackets { get; private set; }
        public USB Device { get; set; }
        public bool IsInitialised = false;
        public Parser.IExpression<CanPacket> FilterExpression;

        private bool _isCapturing = true;
        public bool IsCapturing
        {
            get
            {
                return _isCapturing;
            }
            private set
            {
                _isCapturing = value;
                RaisePropertyChanged();
            }
        }
        private int _packetCount = 0;
        public int PacketCount { get { return _packetCount; } private set { _packetCount = value; RaisePropertyChanged("PacketCount"); } }

        /// <summary>
        /// Maximum count of packets
        /// </summary>
        private const int PACKET_MAX = 400;
        /// <summary>
        /// When is too much packets, they are reduced to this value
        /// </summary>
        private const int PACKET_REDUCED = 350;
        private List<CanPacket> PacketBuffer = new List<CanPacket>();
        private System.Windows.Threading.Dispatcher GUIDispatcher;


        public MainController(System.Windows.Threading.Dispatcher dispatcher)
        {
            this.GUIDispatcher = dispatcher;

            /* Initialisation */
            this.ViewPackets = new ObservableCollection<CanPacket>();
#if PREFILL
            this.AddPacket(new CanPacket(true, 8, 3, CanPacket.CAN_identifier_type.CAN_Id_Extended, CanPacket.CAN_remote_transmission_request.CAN_RTR_Data, 4, new byte[] { 1, 2, 3, 4, 1, 2, 3, 4 }, 80));
            this.AddPacket(new CanPacket(true, 18, 0x1FFFFFFF, CanPacket.CAN_identifier_type.CAN_Id_Standard, CanPacket.CAN_remote_transmission_request.CAN_RTR_Remote, 8, new byte[] { 65, 027, 3, 078, 1, 2, 3, 4 }, 27));
            this.AddPacket(new CanPacket(false, 8, 31, CanPacket.CAN_identifier_type.CAN_Id_Standard, CanPacket.CAN_remote_transmission_request.CAN_RTR_Data, 1, new byte[] { 1, 2, 3, 4, 1, 45, 3, 4 }, 0));
            this.AddPacket(new CanPacket(true, 8, 13, CanPacket.CAN_identifier_type.CAN_Id_Extended, CanPacket.CAN_remote_transmission_request.CAN_RTR_Remote, 5, new byte[] { 45, 2, 3, 4, 45, 2, 3, 4 }, 78));
            this.AddPacket(new CanPacket(true, 128, 123, CanPacket.CAN_identifier_type.CAN_Id_Standard, CanPacket.CAN_remote_transmission_request.CAN_RTR_Data, 4, new byte[] { 1, 45, 3, 4, 1, 2, 3, 4 }, 123));
            this.AddPacket(new CanPacket(false, 8, 23, CanPacket.CAN_identifier_type.CAN_Id_Standard, CanPacket.CAN_remote_transmission_request.CAN_RTR_Remote, 6, new byte[] { 1, 2, 3, 4, 1, 2, 3, 4 }, 11));
            this.AddPacket(new CanPacket(true, 28, 3, CanPacket.CAN_identifier_type.CAN_Id_Extended, CanPacket.CAN_remote_transmission_request.CAN_RTR_Remote, 4, new byte[] { 1, 23, 3, 4, 1, 2, 3, 4 }, 20));
            this.AddPacket(new CanPacket(true, 8, 83, CanPacket.CAN_identifier_type.CAN_Id_Standard, CanPacket.CAN_remote_transmission_request.CAN_RTR_Data, 7, new byte[] { 1, 2, 3, 44, 1, 2, 3, 4 }, 13));
#endif
            this.USBInit();
            if (this.Device != null)
            {
                this.SetTiming();
                this.Device.PacketReceived += new USB.PacketReceivedDelegate(Device_PacketReceived);
                this.SetCaptureState(true);
            }
        }

        /// <summary>
        /// Sets timing values from settings, errors are ignored
        /// </summary>
        private void SetTiming()
        {
            try
            {
                this.Device.SetCommunication(Properties.Settings.Default.Prescaler, Properties.Settings.Default.SJW, Properties.Settings.Default.BS1, Properties.Settings.Default.BS2);
            }
            catch (Exception)
            { }
        }

        private void Device_PacketReceived(object sender, CanPacket packet)
        {
            this.GUIDispatcher.BeginInvoke(new Action(() => { this.AddPacket(packet); }));
        }

        public void SetCaptureState(bool state)
        {
            if (state)
                Device.StartCapture();
            else
                Device.StopCapture();
            IsCapturing = state;

        }

        public void AddPacket(CanPacket canPacket)
        {
            this.PacketBuffer.Add(canPacket);
            this.PacketCount++;
            if (this.PacketCount > PACKET_MAX)
            {
                var Buf = new List<CanPacket>();
                for (int i = this.PacketCount - PACKET_REDUCED; i < this.PacketCount; i++)
                    Buf.Add(this.PacketBuffer[i]);

                this.PacketBuffer = Buf;
                this.PacketCount = Buf.Count;
                this.FilterRefresh();
            }
            else
            {
                if (FilterTest(canPacket))
                {
                    this.ViewPackets.Add(canPacket);
                }
            }
        }

        public void Clear()
        {
            this.ViewPackets.Clear();
            this.PacketBuffer.Clear();

        }

        private void FilterRefresh()
        {
            this.ViewPackets.Clear();
            foreach (var p in this.PacketBuffer)
            {
                if (FilterTest(p))
                    this.ViewPackets.Add(p);
            }

        }

        /// <summary>
        /// Test packets
        /// </summary>
        /// <param name="canPacket"></param>
        /// <returns></returns>
        private bool FilterTest(CanPacket canPacket)
        {
            if (FilterExpression == null)
                return true;
            return FilterExpression.TestValidity(canPacket);
        }

        private void USBInit()
        {
            try
            {
                this.Device = new USB();
                this.IsInitialised = true;
            }
            catch (DeviceNotReadyException)
            {
                this.Device = null;
                if (MessageBox.Show("Device is not ready to communicate. Retry?", "Device not ready", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes)
                    USBInit();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }

        internal void AddFilter(string p)
        {
            var lex = new Parser.LexAnalyzer();
            lex.Parse(p);
            var analyzer = new Parser.LLAnalyzer<CanPacket>(lex, new PropertyFinder());
           this.FilterExpression= analyzer.Analyze();
           this.FilterRefresh();
        }

        internal void ClearFilter()
        {
            this.FilterExpression = null;
            this.FilterRefresh();
        }

        internal void ShowFilter()
        {
            string filter = "none";
            if (this.FilterExpression != null)
            {
                filter = FilterExpression.ToString();
            }
            MessageBox.Show("Current filter: \n" + filter, "Filter", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
