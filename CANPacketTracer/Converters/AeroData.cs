﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CAN2USB;

namespace CANPacketTracer.Converters
{
    public class AeroData : IValueConverter
    {
        private byte[] data;
        static UInt32 LastDoubleL, LastDoubleH;
        static bool IsLastDoubleL = false, IsLastDoubleH = false;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            var packet = value as CanPacket;
            data = packet.Data;
            StringBuilder s = new StringBuilder();

            for (int i = 0; i < 4; i++)
            {
                // In limit
                if (packet.DLC > i + 4)
                {
                    s.Append(data[i + 4].ToString("X").PadLeft(2, '0'));
                }
                else
                    s.Append("  ");
            }
            s.Append(" ");

            byte datatype = data[1];

            if (datatype == 0x00)
                s.Append(" no data");
            else if (datatype == 0x01)
            {
                s.Append("EC=");
                s.Append(GetShort(4));
                s.Append(", OID=");
                s.Append((sbyte)data[6]);
                s.Append(", LID=");
                s.Append((sbyte)data[7]);
            }
            else if (datatype == 0x02) // FLOAT
                s.Append(GetFloat(4));
            else if (datatype == 0x03) // LONG
                s.Append(GetLong(4));
            else if (datatype == 0x04) // ULONG
                s.Append((ulong)GetLong(4));
            else if (datatype == 0x05) // BLONG
                s.Append(ToBin(GetLong(4)));
            else if (datatype == 0x06) // SHORT
                s.Append(GetShort(4));
            else if (datatype == 0x07) // USHORT
                s.Append((ushort)GetShort(4));
            else if (datatype == 0x08) // BSHORT
                s.Append(ToBin(GetShort(4)));
            else if (datatype == 0x09) // CHAR 
                s.Append((sbyte)data[4]);
            else if (datatype == 0x0A) // UCHAR
                s.Append(data[4]);
            else if (datatype == 0x0B) // BCHAR
                s.Append(ToBin(data[4]));
            else if (datatype == 0x0C) // SHORT2
            {
                s.Append("1=");
                s.Append(GetShort(4));
                s.Append(", 2=");
                s.Append(GetShort(6));
            }
            else if (datatype == 0x0D) // USHORT2
            {
                s.Append("1=");
                s.Append((ushort)GetShort(4));
                s.Append(", 2=");
                s.Append((ushort)GetShort(6));
            }
            else if (datatype == 0x0E) // BSHORT2
            {
                s.Append("1=");
                s.Append(ToBin(GetShort(4)));
                s.Append(", 2=");
                s.Append(ToBin(GetShort(6)));
            }
            else if (datatype == 0x0F) // CHAR4
            {
                s.Append("1=");
                s.Append((sbyte)data[4]);
                s.Append(", 2=");
                s.Append((sbyte)data[5]);
                s.Append(", 3=");
                s.Append((sbyte)data[6]);
                s.Append(", 4=");
                s.Append((sbyte)data[7]);
            }
            else if (datatype == 0x10) // UCHAR4
            {
                s.Append("1=");
                s.Append(data[4]);
                s.Append(", 2=");
                s.Append(data[5]);
                s.Append(", 3=");
                s.Append(data[6]);
                s.Append(", 4=");
                s.Append(data[7]);
            }
            else if (datatype == 0x11) // BCHAR4
            {
                s.Append("1=");
                s.Append(ToBin(data[4]));
                s.Append(", 2=");
                s.Append(ToBin(data[5]));
                s.Append(", 3=");
                s.Append(ToBin(data[6]));
                s.Append(", 4=");
                s.Append(ToBin(data[7]));
            }
            else if (datatype == 0x12) // CHAR2
            {
                s.Append("1=");
                s.Append((sbyte)data[4]);
                s.Append(", 2=");
                s.Append((sbyte)data[5]);
            }
            else if (datatype == 0x13) // UCHAR2
            {
                s.Append("1=");
                s.Append(data[4]);
                s.Append(", 2=");
                s.Append(data[5]);
            }
            else if (datatype == 0x14) // BCHAR2
            {
                s.Append("1=");
                s.Append(ToBin(data[4]));
                s.Append(", 2=");
                s.Append(ToBin(data[5]));
            }
            else if (datatype == 0x15) // MEMID
                s.Append((ulong)GetLong(4));
            else if (datatype == 0x16) // CHKSUM
                s.Append((ulong)GetLong(4));
            else if (datatype == 0x17) // ACHAR
            {
                s.Append((char)(data[4]));
            }
            else if (datatype == 0x18) // ACHAR2
            {
                s.Append((char)(data[4]));
                s.Append((char)(data[5]));
            }
            else if (datatype == 0x19) // ACHAR4
            {
                s.Append((char)(data[4]));
                s.Append((char)(data[5]));
                s.Append((char)(data[6]));
                s.Append((char)(data[7]));
            }
            else if (datatype == 0x1A) // CHAR3
            {
                s.Append("1=");
                s.Append((sbyte)data[4]);
                s.Append(", 2=");
                s.Append((sbyte)data[5]);
                s.Append(", 3=");
                s.Append((sbyte)data[6]);
            }
            else if (datatype == 0x1B) // UCHAR3
            {
                s.Append("1=");
                s.Append(data[4]);
                s.Append(", 2=");
                s.Append(data[5]);
                s.Append(", 3=");
                s.Append(data[6]);
            }
            else if (datatype == 0x1C) // BCHAR3
            {
                s.Append("1=");
                s.Append(ToBin(data[4]));
                s.Append(", 2=");
                s.Append(ToBin(data[5]));
                s.Append(", 3=");
                s.Append(ToBin(data[6]));
            }
            else if (datatype == 0x1D) // ACHAR3
            {
                s.Append((char)(data[4]));
                s.Append((char)(data[5]));
                s.Append((char)(data[6]));
            }
            else if (datatype == 0x1E) // doubleH
            {
                IsLastDoubleH = true;
                LastDoubleH = (uint)GetLong(4);
                PrintLastDouble(s);
            }
            else if (datatype == 0x1F) // doubleL
            {
                IsLastDoubleL = true;
                LastDoubleL = (uint)GetLong(4);
                PrintLastDouble(s);
                
            }
            //else if (datatype >= 0x20 && datatype <= 0x63)
            //return "RESVD";
            //return "UDEF";
            return s.ToString();
        }

        private static void PrintLastDouble(StringBuilder s)
        {
            if (IsLastDoubleH && IsLastDoubleL)
            {
                long bArr = ((long)LastDoubleH << 32) | LastDoubleL;
                double d = BitConverter.Int64BitsToDouble(bArr);
                s.Append("wt. prev " + d.ToString());
            }
        }

        private long GetLong(int p)
        {
            return (long)(data[p] << 24 |
                data[p + 1] << 16 |
                data[p + 2] << 8 |
                data[p + 3]);
        }

        private float GetFloat(int p)
        {
            return BitConverter.ToSingle(data, p);
        }

        private string GetChar(int p)
        {
            return this.data[p].ToString();
        }

        private short GetShort(int p)
        {
            return (short)(this.data[p] << 8 | this.data[p + 1]);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private static string ToBin(byte b)
        {
            return System.Convert.ToString(b, 2).PadLeft(8, '0');
        }
        private static string ToBin(short b)
        {
            return System.Convert.ToString(b, 2).PadLeft(16, '0');
        }
        private static string ToBin(long b)
        {
            return System.Convert.ToString(b, 2).PadLeft(32, '0');
        }
    }
}
