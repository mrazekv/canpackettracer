﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CAN2USB;

namespace CANPacketTracer.Converters
{
    public class PacketData : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            CanPacket packet = value as CanPacket;
            if (packet == null) return "null";

            StringBuilder hex = new StringBuilder();
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < 8; i++)
            {
                //if (i != 0)
                //    str.Append(" ");
                if (i < packet.DLC)
                {
                  //  str.Append("0x");
                    hex.Append(packet.Data[i].ToString("X").PadLeft(2, '0'));
                    char c = (char)packet.Data[i];
                    if (char.IsControl(c))
                    {
                        str.Append('□');
                    }
                    else
                        str.Append(c);
                }
                else
                {
                    hex.Append("  ");
                    str.Append('■');
                }
            }
            return string.Format("{0} \"{1}\"", hex.ToString(), str.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
