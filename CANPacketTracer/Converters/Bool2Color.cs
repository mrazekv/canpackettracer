﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace CANPacketTracer.Converters
{
    /// <summary>
    /// Returns TrueValue or FalseValue brush on true/false value
    /// </summary>
    public class Bool2Color : IValueConverter
    {
        public Brush TrueValue { get; set; }
        public Brush FalseValue { get; set; }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return System.Convert.ToBoolean(value) ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
