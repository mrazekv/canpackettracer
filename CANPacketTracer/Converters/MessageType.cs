﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace CANPacketTracer.Converters
{
   public class MessageType : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            UInt32 stdId = (UInt32)value;
            if (stdId <= 0x07f)
                return "EED";
            if (stdId <= 0x0c7)
                return "NSH";
            if (stdId <= 0x12b)
                return "UDH";
            if (stdId <= 0x707)
                return "NOD";
            if (stdId <= 0x76b)
                return "UDL";
            if (stdId <= 0x7cf)
                return "DSD";
            if (stdId <= 0x7ef)
                return "NSL";
            return "???";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
