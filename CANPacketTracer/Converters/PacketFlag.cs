﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CAN2USB;

namespace CANPacketTracer.Converters
{
    public enum PacketFlagType {IDE, RTR};
    public class PacketFlag : IValueConverter
    {
        public PacketFlagType FlagType { get; set; }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            CanPacket packet = value as CanPacket                ;
            if (packet == null) return "n";

            if (FlagType == PacketFlagType.IDE)
                return packet.IDE == CanPacket.CAN_identifier_type.CAN_Id_Standard ? "S" : "E";
            return packet.RTR == CanPacket.CAN_remote_transmission_request.CAN_RTR_Data ? "D" : "R";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
