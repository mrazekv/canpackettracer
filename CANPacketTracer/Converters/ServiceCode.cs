﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CAN2USB;

/*
IDS 0
NSS 1
DDS 2
DUS 3
SCS 4
TIS 5
FPS 6
STS 7
FSS 8
TCS 9
BSS 10
NIS 11
MIS 12
MCS 13
CSS 14
DSS 15
XXS 16-99
USR 100-255
*/

namespace CANPacketTracer.Converters
{
    public class ServiceCode:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var packet = value as CanPacket;
            return packet.Service;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
