﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace CANPacketTracer.Converters
{
    /// <summary>
    /// returns unsigned HEX value 
    /// </summary>
    public class Byte2Hex : IValueConverter
    {
        public int Places { get; set; }
        public Byte2Hex()
        {
            Places = 0;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string s;
            UInt32 v;
            try
            {
                v = System.Convert.ToUInt32(value);
            }
            catch (FormatException)
            {
                v = 0;
            }
            s= v.ToString("X");
            if (Places > 0)
                return s.PadLeft(Places, '0');
            return s;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
