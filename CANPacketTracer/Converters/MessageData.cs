﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CAN2USB;


namespace CANPacketTracer.Converters
{
    public class MessageData : IValueConverter
    {
        public int Position { get; set; }
        public MessageData()
        {
            Position = 0;
        }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var packet = value as CanPacket;
            byte data = packet.Data[this.Position];
            return data.ToString("X").PadLeft(2, '0');
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
