﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace CANPacketTracer.Converters
{
    public class Time2String : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var t = (DateTime)value;
            StringBuilder str = new StringBuilder();
            str.Append(t.Hour);
            str.Append(":");
            str.Append(t.Minute.ToString().PadLeft(2, '0'));


            return str.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
