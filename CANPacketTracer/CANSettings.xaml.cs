﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CANPacketTracer
{
    /// <summary>
    /// Interaction logic for CANSettings.xaml
    /// </summary>
    public partial class CANSettings : Window
    {
        private CAN2USB.USB device;


        public CANSettings(CAN2USB.USB dev)
        {
            InitializeComponent();
            this.device = dev;

            txtPrescaler.Text = Properties.Settings.Default.Prescaler.ToString();
            txtSJW.Text = Properties.Settings.Default.SJW.ToString();
            txtBS1.Text = Properties.Settings.Default.BS1.ToString();
            txtBS2.Text = Properties.Settings.Default.BS2.ToString();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte prescaler = Convert.ToByte(txtPrescaler.Text);
                byte SJW = Convert.ToByte(txtSJW.Text);
                byte BS1 = Convert.ToByte(txtBS1.Text);
                byte BS2 = Convert.ToByte(txtBS2.Text);
                device.SetCommunication(prescaler, SJW, BS1, BS2);


                Properties.Settings.Default.Prescaler = prescaler;
                Properties.Settings.Default.SJW = SJW;
                Properties.Settings.Default.BS1 = BS1;
                Properties.Settings.Default.BS2 = BS2;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
