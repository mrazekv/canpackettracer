﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CANPacketTracer
{
    /// <summary>
    /// Interaction logic for AddAero.xaml
    /// </summary>
    public partial class AddAero : Window
    {
        private CAN2USB.USB device;
        private CAN2USB.CanPacket packet;


        public AddAero(CAN2USB.USB device)
        {
            this.device = device;
            InitializeComponent();
            cmbDataType.SelectedIndex = 0;
            cmbServiceType.SelectedIndex = 0;
        }

        #region Show info
        /// <summary>
        /// hides all
        /// </summary>
        public void ShowInfo()
        {
            lblData1.Visibility = Visibility.Hidden;
            txtData1.Visibility = Visibility.Hidden;
            lblData2.Visibility = Visibility.Hidden;
            txtData2.Visibility = Visibility.Hidden;
            lblData3.Visibility = Visibility.Hidden;
            txtData3.Visibility = Visibility.Hidden;
            lblData4.Visibility = Visibility.Hidden;
            txtData4.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 1 text
        /// </summary>
        public void ShowInfo(string desc1)
        {
            lblData1.Visibility = Visibility.Visible;
            txtData1.Visibility = Visibility.Visible;
            lblData1.Content = desc1;
            lblData2.Visibility = Visibility.Hidden;
            txtData2.Visibility = Visibility.Hidden;
            lblData3.Visibility = Visibility.Hidden;
            txtData3.Visibility = Visibility.Hidden;
            lblData4.Visibility = Visibility.Hidden;
            txtData4.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 2 text
        /// </summary>
        public void ShowInfo(string desc1, string desc2)
        {
            lblData1.Visibility = Visibility.Visible;
            txtData1.Visibility = Visibility.Visible;
            lblData1.Content = desc1;
            lblData2.Visibility = Visibility.Visible;
            txtData2.Visibility = Visibility.Visible;
            lblData2.Content = desc2;
            lblData3.Visibility = Visibility.Hidden;
            txtData3.Visibility = Visibility.Hidden;
            lblData4.Visibility = Visibility.Hidden;
            txtData4.Visibility = Visibility.Hidden;
        }


        /// <summary>
        /// 3 text
        /// </summary>
        public void ShowInfo(string desc1, string desc2, string desc3)
        {
            lblData1.Visibility = Visibility.Visible;
            txtData1.Visibility = Visibility.Visible;
            lblData1.Content = desc1;
            lblData2.Visibility = Visibility.Visible;
            txtData2.Visibility = Visibility.Visible;
            lblData2.Content = desc2;
            lblData3.Visibility = Visibility.Visible;
            txtData3.Visibility = Visibility.Visible;
            lblData3.Content = desc3;
            lblData4.Visibility = Visibility.Hidden;
            txtData4.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 4 text
        /// </summary>
        public void ShowInfo(string desc1, string desc2, string desc3, string desc4)
        {
            lblData1.Visibility = Visibility.Visible;
            txtData1.Visibility = Visibility.Visible;
            lblData1.Content = desc1;
            lblData2.Visibility = Visibility.Visible;
            txtData2.Visibility = Visibility.Visible;
            lblData2.Content = desc2;
            lblData3.Visibility = Visibility.Visible;
            txtData3.Visibility = Visibility.Visible;
            lblData3.Content = desc3;
            lblData4.Visibility = Visibility.Visible;
            txtData4.Visibility = Visibility.Visible;
            lblData4.Content = desc4;
        }


        private void cmbDataType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch ((string)((ComboBoxItem)cmbDataType.SelectedItem).Content)
            {
                case "NODATA":
                    ShowInfo();
                    break;
                case "ERROR":
                    ShowInfo("Error code (short)", "Operation ID (char)", "Location ID (char)");
                    break;
                case "FLOAT":
                    ShowInfo("Data (float)");
                    break;
                case "LONG":
                    ShowInfo("Data (signed long)");
                    break;
                case "ULONG":
                    ShowInfo("Data (unsigned long)");
                    break;
                case "BLONG":
                    ShowInfo("Data (32 bit binary)");
                    break;
                case "SHORT":
                    ShowInfo("Data (signed short)");
                    break;
                case "USHORT":
                    ShowInfo("Data (unsigned short)");
                    break;
                case "BSHORT":
                    ShowInfo("Data (16 bit binary)");
                    break;
                case "CHAR":
                    ShowInfo("Data (signed char)");
                    break;
                case "UCHAR":
                    ShowInfo("Data (unsigned char)");
                    break;
                case "BCHAR":
                    ShowInfo("Data (8 bit binary)");
                    break;
                case "SHORT2":
                    ShowInfo("Data (signed short)", "Data (signed short)");
                    break;
                case "USHORT2":
                    ShowInfo("Data (unsigned short)", "Data (unsigned short)");
                    break;
                case "BSHORT2":
                    ShowInfo("Data (16 bit binary)", "Data (16 bit binary)");
                    break;
                case "CHAR4":
                    ShowInfo("Data (signed char)", "Data (signed char)", "Data (signed char)", "Data (signed char)");
                    break;
                case "UCHAR4":
                    ShowInfo("Data (unsigned char)", "Data (unsigned char)", "Data (unsigned char)", "Data (unsigned char)");
                    break;
                case "BCHAR4":
                    ShowInfo("Data (8 bit binary)", "Data (8 bit binary)", "Data (8 bit binary)", "Data (8 bit binary)");
                    break;
                case "CHAR2":
                    ShowInfo("Data (signed char)", "Data (signed char)");
                    break;
                case "UCHAR2":
                    ShowInfo("Data (unsigned char)", "Data (unsigned char)");
                    break;
                case "BCHAR2":
                    ShowInfo("Data (8 bit binary)", "Data (8 bit binary)");
                    break;
                case "MEMID":
                    ShowInfo("Data (hex ulong)");
                    break;
                case "CHKSUM":
                    ShowInfo("Data (hex ulong)");
                    break;
                case "ACHAR":
                    ShowInfo("Data (ascii char)");
                    break;
                case "ACHAR2":
                    ShowInfo("Data (ascii char)", "Data (ascii char)");
                    break;
                case "ACHAR4":
                    ShowInfo("Data (ascii char)", "Data (ascii char)", "Data (ascii char)", "Data (ascii char)");
                    break;
                case "CHAR3":
                    ShowInfo("Data (signed char)", "Data (signed char)", "Data (signed char)");
                    break;
                case "UCHAR3":
                    ShowInfo("Data (unsigned char)", "Data (unsigned char)", "Data (unsigned char)");
                    break;
                case "BCHAR3":
                    ShowInfo("Data (8 bit binary)", "Data (8 bit binary)", "Data (8 bit binary)");
                    break;
                case "ACHAR3":
                    ShowInfo("Data (ascii char)", "Data (ascii char)", "Data (ascii char)");
                    break;
                case "DOUBLEH":
                    ShowInfo("Data (double)");
                    break;
                case "DOUBLEL":
                    ShowInfo("Data (double)");
                    break;
                case "RESVD":
                case "UDEF":
                    ShowInfo("Data (hex ulong, 24-32 bit is data[5] ..) ", "DLC (4-8)");
                    var f = new PromptWindow("Enter own code (HEX) 32-255");
                    while (true)
                    {
                        if (f.ShowDialog() == true)
                        {
                            try
                            {
                                this.DataType = Convert.ToByte(f.Value, 16);
                                break;
                            }
                            catch (Exception) // repeat action
                            { }
                        }
                        f = new PromptWindow("Enter own code (HEX) 20-FF"); // for next
                    }
                    break;
            }

        }
        #endregion

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                packet = new CAN2USB.CanPacket(true);
                packet.StdId = Convert.ToUInt32(txtAddr.Text, 16);
                packet.Data[0] = Convert.ToByte(txtNodeId.Text, 16); // Node ID

                // packet.Data[1]  is DataType
                packet.Data[2] = GetServiceCode((string)((ComboBoxItem)cmbServiceType.SelectedItem).Content); // Service code
                packet.Data[3] = Convert.ToByte(txtMessageCode.Text, 16); // Message ID
                FillData();


                device.SendPacket(this.packet);
            }
            catch (Exception ex)
            {
                ErrHandler.Handle(ex);
            }
        }


        /// <summary>
        /// Fills datatype (Data[1]), DLC, Data[4-7]
        /// </summary>
        private void FillData()
        {
            switch ((string)((ComboBoxItem)cmbDataType.SelectedItem).Content)
            {
                case "NODATA":
                    packet.DLC = 4;
                    packet.Data[1] = 0x00;
                    break;
                case "ERROR":
                    packet.DLC = 8;
                    packet.Data[1] = 0x01;
                    FillShort(txtData1.Text, 4);
                    FillChar(txtData2.Text, 6);
                    FillChar(txtData3.Text, 7);
                    break;
                case "FLOAT":
                    packet.DLC = 8;
                    packet.Data[1] = 0x02;
                    FillFloat(txtData1.Text);
                    break;
                case "LONG":
                    packet.DLC = 8;
                    packet.Data[1] = 0x03;
                    FillLong(txtData1.Text);
                    break;
                case "ULONG":
                    packet.DLC = 8;
                    packet.Data[1] = 0x04;
                    FillULong(txtData1.Text);
                    break;
                case "BLONG":
                    packet.DLC = 8;
                    packet.Data[1] = 0x05;
                    FillBLong(txtData1.Text);
                    break;
                case "SHORT":
                    packet.DLC = 6;
                    packet.Data[1] = 0x06;
                    FillShort(txtData1.Text, 4);
                    break;
                case "USHORT":
                    packet.DLC = 6;
                    packet.Data[1] = 0x07;
                    FillUShort(txtData1.Text, 4);
                    break;
                case "BSHORT":
                    packet.DLC = 6;
                    packet.Data[1] = 0x08;
                    FillBShort(txtData1.Text, 4);
                    break;
                case "CHAR":
                    packet.DLC = 5;
                    packet.Data[1] = 0x09;
                    FillChar(txtData1.Text, 4);
                    break;
                case "UCHAR":
                    packet.DLC = 5;
                    packet.Data[1] = 0x0A;
                    FillUChar(txtData1.Text, 4);
                    break;
                case "BCHAR":
                    packet.DLC = 5;
                    packet.Data[1] = 0x0B;
                    FillBChar(txtData1.Text, 4);
                    break;
                case "SHORT2":
                    packet.DLC = 8;
                    packet.Data[1] = 0x0C;
                    FillShort(txtData1.Text, 4);
                    FillShort(txtData2.Text, 6);
                    break;
                case "USHORT2":
                    packet.DLC = 8;
                    packet.Data[1] = 0x0D;
                    FillUShort(txtData1.Text, 4);
                    FillUShort(txtData2.Text, 6);
                    break;
                case "BSHORT2":
                    packet.DLC = 8;
                    packet.Data[1] = 0x0E;
                    FillBShort(txtData1.Text, 4);
                    FillBShort(txtData2.Text, 6);
                    break;
                case "CHAR4":
                    packet.DLC = 8;
                    packet.Data[1] = 0x0F;
                    FillChar(txtData1.Text, 4);
                    FillChar(txtData2.Text, 5);
                    FillChar(txtData3.Text, 6);
                    FillChar(txtData4.Text, 7);
                    break;
                case "UCHAR4":
                    packet.DLC = 8;
                    packet.Data[1] = 0x10;
                    FillUChar(txtData1.Text, 4);
                    FillUChar(txtData2.Text, 5);
                    FillUChar(txtData3.Text, 6);
                    FillUChar(txtData4.Text, 7);
                    break;
                case "BCHAR4":
                    packet.DLC = 8;
                    packet.Data[1] = 0x11;
                    FillBChar(txtData1.Text, 4);
                    FillBChar(txtData2.Text, 5);
                    FillBChar(txtData3.Text, 6);
                    FillBChar(txtData4.Text, 7);
                    break;
                case "CHAR2":
                    packet.DLC = 6;
                    packet.Data[1] = 0x12;
                    FillChar(txtData1.Text, 4);
                    FillChar(txtData2.Text, 5);
                    break;
                case "UCHAR2":
                    packet.DLC = 6;
                    packet.Data[1] = 0x13;
                    FillUChar(txtData1.Text, 4);
                    FillUChar(txtData2.Text, 5);
                    break;
                case "BCHAR2":
                    packet.DLC = 6;
                    packet.Data[1] = 0x14;
                    FillBChar(txtData1.Text, 4);
                    FillBChar(txtData2.Text, 5);
                    break;
                case "MEMID":
                    packet.DLC = 8;
                    packet.Data[1] = 0x15;
                    FillULongHex(txtData1.Text);
                    break;
                case "CHKSUM":
                    packet.DLC = 8;
                    packet.Data[1] = 0x16;
                    FillULongHex(txtData1.Text);
                    break;
                case "ACHAR":
                    packet.DLC = 5;
                    packet.Data[1] = 0x17;
                    FillAChar(txtData1.Text, 4);
                    break;
                case "ACHAR2":
                    packet.DLC = 6;
                    packet.Data[1] = 0x18;
                    FillAChar(txtData1.Text, 4);
                    FillAChar(txtData2.Text, 5);
                    break;
                case "ACHAR4":
                    packet.DLC = 8;
                    packet.Data[1] = 0x19;
                    FillAChar(txtData1.Text, 4);
                    FillAChar(txtData2.Text, 5);
                    FillAChar(txtData3.Text, 6);
                    FillAChar(txtData4.Text, 7);
                    break;
                case "CHAR3":
                    packet.DLC = 7;
                    packet.Data[1] = 0x1A;
                    FillChar(txtData1.Text, 4);
                    FillChar(txtData2.Text, 5);
                    FillChar(txtData3.Text, 6);
                    break;
                case "UCHAR3":
                    packet.DLC = 7;
                    packet.Data[1] = 0x1B;
                    FillUChar(txtData1.Text, 4);
                    FillUChar(txtData2.Text, 5);
                    FillUChar(txtData3.Text, 6);
                    break;
                case "BCHAR3":
                    packet.DLC = 7;
                    packet.Data[1] = 0x1C;
                    FillBChar(txtData1.Text, 4);
                    FillBChar(txtData2.Text, 5);
                    FillBChar(txtData3.Text, 6);
                    break;
                case "ACHAR3":
                    packet.DLC = 7;
                    packet.Data[1] = 0x1D;
                    FillAChar(txtData1.Text, 4);
                    FillAChar(txtData2.Text, 5);
                    FillAChar(txtData3.Text, 6);
                    break;
                case "DOUBLEH":
                    packet.DLC = 8;
                    packet.Data[1] = 0x1E;
                    FillDouble(txtData1.Text, true);
                    break;
                case "DOUBLEL":
                    packet.DLC = 8;
                    packet.Data[1] = 0x1F;
                    FillDouble(txtData1.Text, false);
                    break;
                case "RESVD":
                case "UDEF":
                    packet.DLC = GetDLC(txtData2.Text);
                    packet.Data[1] = this.DataType;
                    FillULongHex(txtData1.Text);
                    break;
            }
        }

        private byte GetDLC(string data)
        {
            byte d = Convert.ToByte(data);
            if (d > 8 || d < 4)
                throw new FormatException("DLC must be between 4-8");
            return d;
        }

        private void FillDouble(string p, bool isHight)
        {
            double d = Convert.ToDouble(p);
            var data = BitConverter.DoubleToInt64Bits(d);
            if (isHight)
                FillLongData((ulong)(data >> 32));
            else
                FillLongData((ulong)data);
        }

        private void FillLongData(ulong data)
        {
            packet.Data[4] = (byte)(data >> 24);
            packet.Data[5] = (byte)(data >> 16);
            packet.Data[6] = (byte)(data >> 8);
            packet.Data[7] = (byte)(data);
        }

        private void FillULongHex(string p)
        {
            ulong data = Convert.ToUInt32(p, 16);
            FillLongData(data);
        }

        private void FillAChar(string data, int position)
        {
            if (data.Length != 1)
                throw new FormatException("Length of achar data must be 1");
            packet.Data[position] = (byte)data[0];
        }

        private void FillBChar(string data, int position)
        {
            packet.Data[position] = Convert.ToByte(data, 2);
        }

        private void FillUChar(string data, int position)
        {
            packet.Data[position] = Convert.ToByte(data);
        }

        private void FillBShort(string data, int position)
        {
            FillShortData(Convert.ToUInt16(data, 2), position);
        }

        private void FillShortData(ushort data, int position)
        {
            packet.Data[position] = (byte)(data >> 8);
            packet.Data[position + 1] = (byte)(data);
        }

        private void FillUShort(string data, int position)
        {
            FillShortData(Convert.ToUInt16(data), position);
        }

        private void FillULong(string data)
        {
            FillLongData(Convert.ToUInt32(data));
        }

        private void FillBLong(string data)
        {
            FillLongData(Convert.ToUInt32(data, 2));
        }

        private void FillLong(string data)
        {
            FillLongData((ulong)Convert.ToInt32(data));
        }

        private void FillFloat(string data)
        {
            float f = Convert.ToSingle(data);
            var bytes = BitConverter.GetBytes(f);
            packet.Data[4] = bytes[0];
            packet.Data[5] = bytes[1];
            packet.Data[6] = bytes[2];
            packet.Data[7] = bytes[3];
        }

        private void FillChar(string data, int position)
        {
            packet.Data[position] = (byte)(Convert.ToSByte(data));
        }

        private void FillShort(string data, int position)
        {
            FillShortData((ushort)Convert.ToInt16(data), position);
        }

        private byte GetServiceCode(string text)
        {
            switch (text)
            {
                case "IDS":
                    return 0;
                case "NSS":
                    return 1;
                case "DDS":
                    return 2;
                case "DUS":
                    return 3;
                case "SCS":
                    return 4;
                case "TIS":
                    return 5;
                case "FPS":
                    return 6;
                case "STS":
                    return 7;
                case "FSS":
                    return 8;
                case "TCS":
                    return 9;
                case "BSS":
                    return 10;
                case "NIS":
                    return 11;
                case "MIS":
                    return 12;
                case "MCS":
                    return 13;
                case "CSS":
                    return 14;
                case "DSS":
                    return 15;
                case "XXS":
                case "USR":
                    return this.ServiceCode;
            }
            return 0;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void cmbServiceType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string selected = (string)((ComboBoxItem)cmbServiceType.SelectedItem).Content;
            if (selected == "XXS" || selected == "USR")
            {
                var f = new PromptWindow("Enter own code (HEX) 32-255");
                while (true)
                {
                    if (f.ShowDialog() == true)
                    {
                        try
                        {
                            this.ServiceCode = Convert.ToByte(f.Value, 16);
                            break;
                        }
                        catch (Exception) // repeat action
                        { }
                    }
                    f = new PromptWindow("Enter own code (HEX) 10-FF"); // for next
                }
            }
        }

        public byte ServiceCode { get; set; }

        public byte DataType { get; set; }
    }
}
