﻿// Debug variable - prefill wt random packets
#define PREFILL
/**
 * MainWindow
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using CAN2USB;
using Microsoft.Windows.Controls.Ribbon;
using Microsoft.Win32;
using System.IO;

namespace CANPacketTracer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        MainController controller;
        System.Timers.Timer Timer = new System.Timers.Timer();
        System.Timers.Timer TimerKeepAlive = new System.Timers.Timer();

        public MainWindow()
        {
            InitializeComponent();
            this.controller = new MainController(this.Dispatcher);
            this.controller.ViewPackets.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(packets_CollectionChanged);
            this.DataContext = controller;

            // Use timer in case lot of packets
            Timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
            Timer.Interval = 200;
            Timer.Start();

            // Use timer to check device is ready
            TimerKeepAlive.Elapsed += new System.Timers.ElapsedEventHandler(TimerKeepAlive_Elapsed);
            TimerKeepAlive.Interval = 8000; // In device its 2000
            TimerKeepAlive.Start();

            // Load settings to view
            SetViewAero(Properties.Settings.Default.IsAero);
        }

        /// <summary>
        /// Check, that in last 8000 seconds is KeepAlive flag set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TimerKeepAlive_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (!controller.Device.KeepAlive)
                {
                    TimerKeepAlive.Stop();
                    MessageBox.Show("Connection with device has been lost", "Device", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                controller.Device.KeepAlive = false;
            }
            catch (Exception)
            {
                TimerKeepAlive.Stop();
                MessageBox.Show("Connection with device has been lost", "Device", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (elementsChanged)
            {
                elementsChanged = false;
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (controller.ViewPackets.Count > 1)
                    {
                        //lv1.SelectedIndex = lv1.Items.Count - 1;
                        //lv1.ScrollIntoView(lv1.Items[1]);
                        lv1.ScrollIntoView(controller.ViewPackets.Last());
                        //lv1.UpdateLayout();
                    }
                }));

            }
        }




        private bool elementsChanged = false;
        void packets_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            elementsChanged = true;
        }


        private void Window_Closed(object sender, EventArgs e)
        {
            controller.Device.Close();
            Properties.Settings.Default.Save();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Random r = new Random(DateTime.Now.Millisecond);
                controller.Device.SendPacket(new CanPacket(false, (uint)r.Next() & 0x07FF, 0, CanPacket.CAN_identifier_type.CAN_Id_Standard,
                    CanPacket.CAN_remote_transmission_request.CAN_RTR_Data,
                    (byte)(RandomByte(r) % 0x09), new byte[8] { RandomByte(r), (byte)(RandomByte(r) %0x20) , (byte)(RandomByte(r) % 0x10), RandomByte(r),
                        RandomByte(r), RandomByte(r), RandomByte(r), RandomByte(r) }, 0));

            }
            catch (Exception ex)
            {
                ErrHandler.Handle(ex);
            }
        }

        private byte RandomByte(Random r)
        {
            return (byte)r.Next();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you really want to clear collection of packets?", "Clear collection", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                controller.Clear();
            }
        }

        private void btnSendCAN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var f = new AddCAN(controller.Device);
                f.Show();
            }
            catch (Exception ex)
            {
                ErrHandler.Handle(ex);
            }
        }

        /// <summary>
        /// Set view
        /// </summary>
        /// <param name="value">true = aero, false = can </param>
        private void SetViewAero(bool value)
        {
            if (value)
            {
                btnViewAero.Visibility = Visibility.Collapsed;
                btnViewCan.Visibility = Visibility.Visible;
                lv1.View = FindResource("PacketAeroView") as ViewBase;
            }
            else
            {
                btnViewAero.Visibility = Visibility.Visible;
                btnViewCan.Visibility = Visibility.Collapsed;
                lv1.View = FindResource("PacketCanView") as ViewBase;
            }
            Properties.Settings.Default.IsAero = value;
        }
        private void btnViewAero_Click(object sender, RoutedEventArgs e)
        {
            SetViewAero(true);
        }

        private void btnViewCan_Click(object sender, RoutedEventArgs e)
        {
            SetViewAero(false);
        }

        private void btnSendAero_Click(object sender, RoutedEventArgs e)
        {
            var f = new AddAero(controller.Device);
            f.Show();
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                controller.SetCaptureState(false);
            }
            catch (Exception ex)
            {
                ErrHandler.Handle(ex);
            }
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                controller.SetCaptureState(true);
            }
            catch (Exception ex)
            {
                ErrHandler.Handle(ex);
            }
        }

        private void btnCanSettings_Click(object sender, RoutedEventArgs e)
        {
            var f = new CANSettings(controller.Device);
            f.ShowDialog();
        }

        private void btnApplyFilter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtFilter.Text == "")
                {
                    txtFilter.Style = null;
                    controller.ClearFilter();

                }
                else
                {
                    controller.AddFilter(txtFilter.Text);
                    txtFilter.Style = (Style)FindResource("FilterApply");
                }
            }
            catch (Exception ex)
            {
                txtFilter.Style = null;
                ErrHandler.Handle(ex);
            }
        }

        private void txtFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                btnApplyFilter_Click(sender, new RoutedEventArgs());
        }

        private void btnFilterShow_Click(object sender, RoutedEventArgs e)
        {
            //controller.ShowFilter();
            var f = new FilterShow(controller);
            f.ShowDialog();
        }

        private void btnFilterClear_Click(object sender, RoutedEventArgs e)
        {
            txtFilter.Style = null;
            controller.ClearFilter();
        }

        private void btnExportCSV_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
                Converters.AeroData aerodata = new Converters.AeroData();
                if (dlg.ShowDialog() == true)
                {
                    using (StreamWriter writer = new StreamWriter(dlg.FileName))
                    {
                        writer.WriteLine("Source;CAN-ID;MSG-TYPE;DLC;ID;DATA-TYPE;SERVICE-CODE;MESSAGE-CODE;DATA");
                        foreach (var packet in controller.ViewPackets)
                        {
                            writer.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8}", packet.IsTx ?
                                "TX" : "RX",
                                packet.StdId,
                                packet.MsgType,
                                packet.DLC,
                                packet.Data[0],
                                packet.DataType,
                                packet.Service,
                                packet.Data[3],
                                aerodata.Convert(packet, typeof(string), null, null));
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrHandler.Handle(ex);
            }
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This application is for capturing packets on the CAN bus. It has been made by Vojtech Mrazek, student of FIT VUT Brno. For more information read documentation manual.", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnQuick_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string[] cmd = txtQuickSend.Text.Split('\n');
                foreach (string s in cmd)
                {
                    string c = s.Trim();
                    if (c == "")
                        continue;

                    if (!c.Contains(":"))
                        throw new FormatException("No : has been found in text");

                    string[] c_ar = c.Split(':');
                    if (c_ar.Length != 2)
                        throw new FormatException("Too much parts in quick string " + c);

                    CanPacket packet = new CanPacket(true);
                    packet.StdId = Convert.ToUInt32(c_ar[0], 16);

                    if (c_ar.Length % 2 == 1)
                        throw new FormatException("Length of command must be even");

                    packet.DLC = 0;
                    for (int i = 0; i < c_ar[1].Length; i += 2)
                    {
                        if (i >= 16) throw new FormatException("Data are too long");
                        packet.Data[i / 2] = Convert.ToByte(c_ar[1].Substring(i, 2), 16);
                        packet.DLC++;
                    }

                    controller.Device.SendPacket(packet);
                }
            }
            catch (Exception ex)
            {
                ErrHandler.Handle(ex);
            }
        }
    }
}
