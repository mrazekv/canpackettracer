﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAN2USB
{
    public class CanPacket : Parser.ITestable
    {
        public enum CAN_identifier_type
        {
            /// <summary>
            /// Standard id
            /// </summary>
            CAN_Id_Standard = 0x00000000,
            /// <summary>
            /// Extended id
            /// </summary>
            CAN_Id_Extended = 0x00000004
        };

        public enum CAN_remote_transmission_request
        {
            /// <summary>
            /// Data frame
            /// </summary>
            CAN_RTR_Data = 0x00000000 /*!< Data frame */,
            /// <summary>
            /// Remote frame
            /// </summary>
            CAN_RTR_Remote = 0x00000002  /*!< Remote frame */
        };

        /// <summary>
        /// Specifies the standard identifier.
        /// This parameter can be a value between 0 to 0x7FF.
        /// </summary>
        public UInt32 StdId { get; set; }

        /// <summary>
        /// Specifies the extended identifier.
        /// This parameter can be a value between 0 to 0x1FFFFFFF.
        /// </summary>
        public UInt32 ExtId { get; set; }

        /// <summary>
        /// Specifies the type of identifier for the message that will be received.
        /// </summary>
        public CAN_identifier_type IDE { get; set; }

        /// <summary>
        /// Specifies the type of frame for the received message.
        /// </summary>
        public CAN_remote_transmission_request RTR { get; set; }

        /// <summary>
        /// Specifies the length of the frame that will be received.
        /// This parameter can be a value between 0 to 8 
        /// </summary>
        public byte DLC { get; set; }

        private byte[] _data = new byte[8];

        /// <summary>
        /// Contains the data to be received. It ranges from 0 to 0xFF
        /// </summary>
        public byte[] Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value.Length != 8)
                    throw new ArgumentException("Length of data must be lower than 8");
                _data = value;
            }
        } /*!< Contains the data to be received. It ranges from 0 to 0xFF. */

        public DateTime Time { get; set; }

        /// <summary>
        /// Specifies the index of the filter the message stored in 
        /// the mailbox passes through. This parameter can be a 
        /// value between 0 to 0xFF
        /// </summary>
        public byte FMI { get; set; }
        public bool IsTx { get; set; }

        public CanPacket(bool isTx, UInt32 stdId, UInt32 extId, CAN_identifier_type ide, CAN_remote_transmission_request rtr, byte dlc, byte[] data, byte fmi)
        {
            this.IsTx = isTx;
            this.StdId = stdId;
            this.ExtId = extId;
            this.IDE = ide;
            this.RTR = rtr;
            this.DLC = dlc;
            this.Data = data;
            this.FMI = fmi;
            this.s = 0;
            this.Time = DateTime.Now;
        }

        public CanPacket(bool isTx)
        {
            this.IsTx = isTx;
            this.Time = DateTime.Now;
        }

        public int s { get; set; }

        string Parser.ITestable.GetString(int id)
        {
            /*
             * msgType	STRING	Hodnota EED,NSH atd.	1
             * service	STRING	Typ služby – zkratka	2
             * data	STRING	Datový typ – zkratka	    3
             * */
            switch (id)
            {
                case 1: return this.MsgType;
                case 2: return this.Service;
                case 3: return this.DataType;
            }

            return "";
        }

        int Parser.ITestable.GetInteger(int id)
        {
            /*
             * stdID	UCHAR	Identifikátor CAN	1
             * DLC	UCHAR	Délka paketu	2
             * ID	UCHAR	Node ID (Data.0)	3
             * serviceCode	UCHAR	Typ služby –  kód	4	
             * dataType	UCHAR	Datový typ –kód	5
             * msgCode	UCHAR	Kód zprávy (Data.3)	6
             * DN	UCHAR	N tý bit dat – počítáno od 0 do 7	7-15
             * */
            switch (id)
            {
                case 1:
                    return (int)StdId;
                case 2:
                    return (int)DLC;
                case 3:
                    return (int)Data[0];
                case 4:
                    return (int)Data[2];
                case 5:
                    return (int)Data[1];
                case 6:
                    return (int)Data[3];
            }
            for (int i = 7; i < 16; i++)
            {
                if (id == i)
                    return (int)Data[i - 7];
            }

            return 0;
        }

        public string MsgType
        {
            get
            {
                if (StdId <= 0x07f)
                    return "EED";
                if (StdId <= 0x0c7)
                    return "NSH";
                if (StdId <= 0x12b)
                    return "UDH";
                if (StdId <= 0x707)
                    return "NOD";
                if (StdId <= 0x76b)
                    return "UDL";
                if (StdId <= 0x7cf)
                    return "DSD";
                if (StdId <= 0x7ef)
                    return "NSL";
                return "???";
            }

        }

        public string Service
        {
            get
            {
                byte data = Data[2];
                if (data == 0)
                    return "IDS";
                if (data == 1)
                    return "NSS";
                if (data == 2)
                    return "DDS";
                if (data == 3)
                    return "DUS";
                if (data == 4)
                    return "SCS";
                if (data == 5)
                    return "TIS";
                if (data == 6)
                    return "FPS";
                if (data == 7)
                    return "STS";
                if (data == 8)
                    return "FSS";
                if (data == 9)
                    return "TCS";
                if (data == 10)
                    return "BSS";
                if (data == 11)
                    return "NIS";
                if (data == 12)
                    return "MIS";
                if (data == 13)
                    return "MCS";
                if (data == 14)
                    return "CSS";
                if (data == 15)
                    return "DSS";
                if (data >= 16 && data <= 99)
                    return "XXS (" + data.ToString("X") + ")";

                return "USR (" + data.ToString("X") + ")";
            }
        }

        public string DataType
        {
            get
            {
                byte data = Data[1];

                if (data == 0x00)
                    return "NODATA";
                if (data == 0x01)
                    return "ERROR";
                if (data == 0x02)
                    return "FLOAT";
                if (data == 0x03)
                    return "LONG";
                if (data == 0x04)
                    return "ULONG";
                if (data == 0x05)
                    return "BLONG";
                if (data == 0x06)
                    return "SHORT";
                if (data == 0x07)
                    return "USHORT";
                if (data == 0x08)
                    return "BSHORT";
                if (data == 0x09)
                    return "CHAR";
                if (data == 0x0A)
                    return "UCHAR";
                if (data == 0x0B)
                    return "BCHAR";
                if (data == 0x0C)
                    return "SHORT2";
                if (data == 0x0D)
                    return "USHORT2";
                if (data == 0x0E)
                    return "BSHORT2";
                if (data == 0x0F)
                    return "CHAR4";
                if (data == 0x10)
                    return "UCHAR4";
                if (data == 0x11)
                    return "BCHAR4";
                if (data == 0x12)
                    return "CHAR2";
                if (data == 0x13)
                    return "UCHAR2";
                if (data == 0x14)
                    return "BCHAR2";
                if (data == 0x15)
                    return "MEMID";
                if (data == 0x16)
                    return "CHKSUM";
                if (data == 0x17)
                    return "ACHAR";
                if (data == 0x18)
                    return "ACHAR2";
                if (data == 0x19)
                    return "ACHAR4";
                if (data == 0x1A)
                    return "CHAR3";
                if (data == 0x1B)
                    return "UCHAR3";
                if (data == 0x1C)
                    return "BCHAR3";
                if (data == 0x1D)
                    return "ACHAR3";
                if (data == 0x1E)
                    return "DOUBLEH";
                if (data == 0x1F)
                    return "DOUBLEL";
                if (data >= 0x20 && data <= 0x63)
                    return "RESVD (" + data.ToString("X") + ")";
                return "UDEF (" + data.ToString("X") + ")";
            }
        }
    }
}
