﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibUsbDotNet.Main;
using LibUsbDotNet;
using System.Diagnostics;

namespace CAN2USB
{
    public class USB
    {
        public static UsbDeviceFinder MyUsbFinder = new UsbDeviceFinder(0x0304, 0xE457);
        private UsbDevice device;
        private UsbEndpointWriter writer;
        private UsbEndpointReader reader;
        public CanFilter[] Filters { get; set; }

        private const int InputOffset = 0;

        public bool KeepAlive { get; set; }

        public USB()
        {
            //ErrorCode ec = ErrorCode.None;
            device = UsbDevice.OpenUsbDevice(MyUsbFinder);


            if (device == null)
                throw new DeviceNotReadyException();

            try
            {
                Debug.WriteLine("Is open: {0}", device.IsOpen);
                IUsbDevice wholeUsbDevice = device as IUsbDevice;
                if (!ReferenceEquals(wholeUsbDevice, null))
                {
                    // This is a "whole" USB device. Before it can be used, 
                    // the desired configuration and interface must be selected.

                    // Select config #1
                    wholeUsbDevice.SetConfiguration(1);

                    // Claim interface #0.
                    wholeUsbDevice.ClaimInterface(0);
                }
                writer = device.OpenEndpointWriter(WriteEndpointID.Ep01);

                reader = device.OpenEndpointReader(ReadEndpointID.Ep01);
                reader.DataReceived += new EventHandler<EndpointDataEventArgs>(reader_DataReceived);
                reader.DataReceivedEnabled = true;

            }
            finally
            {
                DefaultFilter();

            }
        }

        /// <summary>
        /// Sets filters to default position
        /// </summary>
        private void DefaultFilter()
        {
            Filters = new CanFilter[14];
            for (int i = 0; i < 14; i++)
                Filters[i] = new CanFilter((byte)i);

            Filters[0].IsSet = true;
            Filters[0].Mode = FilterMode.Mask;

            SendOneByte(PROT_CMD_FILT_RESET);
        }

        public delegate void PacketReceivedDelegate(object sender, CanPacket packet);
        public event PacketReceivedDelegate PacketReceived;

        #region Protocol definition
        private const byte PROT_CMD_SEND = 0x01;
        private const int PROT_LEN_SEND = 14;
        private const byte PROT_CMD_START = 0x03;
        private const int PROT_LEN_START = 1;
        private const byte PROT_CMD_STOP = 0x02;
        private const int PROT_LEN_STOP = 1;
        private const byte PROT_CMD_SET = 0x04;
        private const int PROT_LEN_SET = 5;
        private const byte PROT_CMD_KA = 0x05;
        private const int PROT_LEN_KA = 1;
        private const byte PROT_CMD_FILT_ADD = 0x06;
        private const int PROT_LEN_FILT_ADD = 19;
        private const byte PROT_CMD_FILT_OFF = 0x07;
        private const int PROT_LEN_FILT_OFF = 2;
        private const byte PROT_CMD_FILT_RESET = 0x08;
        private const int PROT_LEN_FILT_RESET = 1;
        #endregion

        void reader_DataReceived(object sender, EndpointDataEventArgs e)
        {
            Debug.WriteLine("Received data " + e.Count.ToString());
            int pos = 0;
            while (pos < e.Count)
            {
                // Data received
                if (e.Count >= pos + PROT_LEN_SEND && e.Buffer[pos + 0] == PROT_CMD_SEND)
                {
                    // Parse packet
                    CanPacket packet = new CanPacket(false);
                    packet.StdId = (uint)(e.Buffer[pos + 4] |
                        e.Buffer[pos + 3] << 8 |
                        e.Buffer[pos + 2] << 16 |
                        e.Buffer[pos + 1] << 24);
                    packet.DLC = e.Buffer[pos + 5];
                    for (int i = 0; i < 8; i++)
                        packet.Data[i] = e.Buffer[pos + 6 + i];

                    // Send packet
                    if (PacketReceived != null)
                        PacketReceived(this, packet);

                    pos += PROT_LEN_SEND;
                    this.KeepAlive = true;
                    Debug.WriteLine("KeepAlive is set");
                }
                // Keep alive packet
                else if (e.Count >= pos + PROT_LEN_KA && e.Buffer[pos + 0] == PROT_CMD_KA)
                {
                    this.KeepAlive = true;
                    Debug.WriteLine("KeepAlive is set");
                    pos += PROT_LEN_KA;
                }
                else
                    return;
            }
        }

        /// <summary>
        /// Send packet to the device
        /// </summary>
        /// <param name="packet"></param>
        /// <exception cref="WriteException">Data se nepodarilo odeslat</exception>
        public void SendPacket(CanPacket packet)
        {
            ErrorCode ec;
            int len;
            var data = new byte[PROT_LEN_SEND] { PROT_CMD_SEND, (byte)(packet.StdId >> 24), (byte)(packet.StdId >> 16), (byte)(packet.StdId >> 8), (byte)(packet.StdId), packet.DLC, 0, 0, 0, 0, 0, 0, 0, 0 };
            // Fill data
            for (int i = 0; i < 8 && i < packet.Data.Length; i++)
                data[i + 6] = packet.Data[i];

            // Alert sending packet
            packet.IsTx = true;
            if (PacketReceived != null)
                PacketReceived(this, packet);

            // Send data
            ec = writer.Write(data, 1000, out len);

            if (ec != ErrorCode.Ok)
                throw new WriteException();

        }

        /// <summary>
        /// Stops capturing from the device
        /// </summary>
        public void StopCapture()
        {
            SendOneByte(PROT_CMD_STOP);
        }

        /// <summary>
        /// Send one byte command to the device
        /// </summary>
        /// <param name="command"></param>
        private void SendOneByte(byte command)
        {
            int len = 0;
            this.writer.Write(new byte[] { command }, 1000, out len);
        }

        /// <summary>
        /// Starts capturing from the device
        /// </summary>
        public void StartCapture()
        {
            SendOneByte(PROT_CMD_START);
        }

        /// <summary>
        /// Set communication parameters for CANBus
        /// </summary>
        /// <param name="SJW">
        /// Specifies the maximum number of time quanta the CAN hardware 
        /// is allowed to lengthen or shorten a bit to perform resynchronization. 
        /// Maximum value is 0x03
        /// </param>
        /// <param name="BS1">
        /// Specifies the number of time quanta in Bit Segment 1. Maximum value is 0x0F
        /// </param>
        /// <param name="BS2">
        /// Specifies the number of time quanta in Bit Segment 2
        /// Maximum value is 0x07
        /// </param>
        public void SetCommunication(byte prescaler, byte SJW, byte BS1, byte BS2)
        {
            if (SJW > 0x03 || BS1 > 0x0F || BS2 > 0x07)
                throw new ArgumentException("Maximum values of time quanta exceded");
            var data = new byte[PROT_LEN_SET] { PROT_CMD_SET, prescaler, SJW, BS1, BS2 };
            int len = 0;
            this.writer.Write(data, 1000, out len);
            this.DefaultFilter();
        }

        public void Close()
        {
            reader.DataReceived -= new EventHandler<EndpointDataEventArgs>(reader_DataReceived);
            device.Close();
        }

        /// <summary>
        /// Removes filter with selected ID from device (disables him)
        /// </summary>
        /// <param name="FilterNumber"></param>
        public void RemoveFilter(int FilterNumber)
        {
            this.Filters[FilterNumber] = new CanFilter((byte)FilterNumber);

            int len = 0;
            this.writer.Write(new byte[] { PROT_CMD_FILT_OFF, (byte)FilterNumber }, 1000, out len);
        }

        /// <summary>
        /// Adds filter to device
        /// </summary>
        /// <param name="filter"></param>
        public void SetFilter(CanFilter filter)
        {
            this.Filters[filter.ID] = filter;

            byte[] data = new byte[PROT_LEN_FILT_ADD];
            data[0] = PROT_CMD_FILT_ADD;
            data[1] = filter.ID;

            data[2] = (byte)(filter.FD1 >> 24);
            data[3] = (byte)(filter.FD1 >> 16);
            data[4] = (byte)(filter.FD1 >> 8);
            data[5] = (byte)(filter.FD1);


            data[6] = (byte)(filter.FD2 >> 24);
            data[7] = (byte)(filter.FD2 >> 16);
            data[8] = (byte)(filter.FD2 >> 8);
            data[9] = (byte)(filter.FD2);

            data[10] = (byte)(filter.FM1 >> 24);
            data[11] = (byte)(filter.FM1 >> 16);
            data[12] = (byte)(filter.FM1 >> 8);
            data[13] = (byte)(filter.FM1);

            data[14] = (byte)(filter.FM2 >> 24);
            data[15] = (byte)(filter.FM2 >> 16);
            data[16] = (byte)(filter.FM2 >> 8);
            data[17] = (byte)(filter.FM2);

            data[18] = (byte)filter.Mode;


            int len = 0;
            this.writer.Write(data, 1000, out len);
        }
    }

    [Serializable]
    public class DeviceNotReadyException : ApplicationException
    {
        public DeviceNotReadyException() { }
        public DeviceNotReadyException(string message) : base(message) { }
        public DeviceNotReadyException(string message, Exception inner) : base(message, inner) { }
        protected DeviceNotReadyException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class WriteException : ApplicationException
    {
        public WriteException() { }
        public WriteException(string message) : base(message) { }
        public WriteException(string message, Exception inner) : base(message, inner) { }
        protected WriteException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
