﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace CAN2USB
{
    public class PropertyFinder : Parser.IPropertyFinder
    {
        /*
             * stdID	UCHAR	Identifikátor CAN	1
             * DLC	UCHAR	Délka paketu	2
             * ID	UCHAR	Node ID (Data.0)	3
             * serviceCode	UCHAR	Typ služby –  kód	4	
             * dataType	UCHAR	Datový typ –kód	5
             * msgCode	UCHAR	Kód zprávy (Data.3)	6
             * DN	UCHAR	N tý bit dat – počítáno od 0 do 7	7-15
             * */

        public int GetId(string name, out bool isString)
        {
            isString = false;
            switch(name)
            {
                case "STDID": return 1;
                case "DLC": return 2;
                case "ID": return 3;
                case "SERVICECODE": return 4;
                case "DATATYPE": return 5;
                case "MSGCODE": return 6;
                case "D0": return 7;
                case "D1": return 8;
                case "D2": return 9;
                case "D3": return 10;
                case "D4": return 11;
                case "D5": return 12;
                case "D6": return 13;
            }
            isString = true;
            /*
             * msgType	STRING	Hodnota EED,NSH atd.	1
             * service	STRING	Typ služby – zkratka	2
             * data	STRING	Datový typ – zkratka	    3
             * */
            switch(name)
            {
                case "MSGTYPE": return 1;
                case "SERVICE": return 2;
                case "DATA": return 3;
            }

            throw new Parser.PropertyNotFoundException("Property " + name + " hasnt not found");
        }
    }
}
